<?php
require_once('animal.php');
require_once('sungkong.php');
require_once('kodok.php');

$sheep = new Animal("shaun");
echo "Nama Animal : ".$sheep->name."<br>"; // "shaun"
echo "Jumlah Kaki : ".$sheep->legs."<br>"; // 4
echo "Cold Blooded : ".$sheep->cold_blooded."<br><br>"; // "no"

$sungkong = new Ape("kera sakti");
echo "Nama Animal : ".$sungkong->name."<br>";
echo "Jumlah Kaki : ".$sungkong->legs."<br>"; 
echo "Cold Blooded : ".$sungkong->cold_blooded."<br>"; 
echo $sungkong->yell("Auooo")."<br><br>";

$kodok = new Frog("buduk");
echo "Nama Animal : ".$kodok->name."<br>"; 
echo "Jumlah Kaki : ".$kodok->legs."<br>"; 
echo "Cold Blooded : ".$kodok->cold_blooded."<br>"; 
echo $kodok->jump("Hop Hop");

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>